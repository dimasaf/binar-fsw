const optionChoice = document.querySelectorAll("#rock, #scissor, #paper");
const comChoice = document.querySelectorAll(
  "#rockComputer, #scissorComputer, #paperComputer"
);
const refreshGame = document.getElementById("refresh");
const resultStatus = document.getElementById("status");
const styleStatus = document.getElementsByClassName("text-status");

function main() {
  optionChoice.forEach((option) => {
    // click option
    option.onclick = () => {
      const userInput = option.id;
      const computerOptions = [
        "rockComputer",
        "scissorComputer",
        "paperComputer",
      ];
      const computerInput = computerOptions[Math.floor(Math.random() * 3)];
      const bgComputer = document.getElementById(`${computerInput}`);

      optionChoice.forEach((item) => {
        if (item.classList.contains("bgcolor")) {
          item.classList.remove("bgcolor");
        }
      });
      comChoice.forEach((item) => {
        if (item.classList.contains("bgcolor")) {
          item.classList.remove("bgcolor");
        }
      });

      bgComputer.classList.add("bgcolor");
      option.classList.add("bgcolor");

      compareResults(userInput, computerInput);
    };

    // click refresh
    refreshGame.onclick = () => {
      resultStatus.innerHTML = "VS";
      resultStatus.className = "text-status";

      optionChoice.forEach((item) => item.classList.remove("bgcolor"));
      comChoice.forEach((item) => item.classList.remove("bgcolor"));
    };
  });
}

const compareResults = (uInput, cInput) => {
  switch (uInput + cInput) {
    case "rockscissorComputer":
    case "scissorpaperComputer":
    case "paperrockComputer":
      resultStatus.innerHTML = "Player 1 Win";
      resultStatus.className = "status-new-style";
      break;
    case "scissorrockComputer":
    case "paperscissorComputer":
    case "rockpaperComputer":
      resultStatus.innerHTML = "Com Win";
      resultStatus.className = "status-new-style";
      break;
    case "paperpaperComputer":
    case "scissorscissorComputer":
    case "rockrockComputer":
      resultStatus.innerHTML = "Draw";
      resultStatus.className = "status-new-style";
      break;
  }
  return resultStatus;
};

main();
