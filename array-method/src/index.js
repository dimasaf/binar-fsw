document.getElementById("app").innerHTML = `
<h1>Method in JavaScript!</h1>`;

let fruits = ["apple", "orange", "rambutan", "durian"];

// 1
const reverse = (array) => [...array].reverse();
const first = reverse(fruits);
// const first = fruits.reverse((item) => item);

// 2
const second = fruits.filter((item) => item === "orange");

// 3
const third = fruits.map((item) => item);
third.splice(2, 1, "strawbery");

// console.warn(fruits)
// console.warn(first)

document.getElementById("wrap").innerHTML = `
<h2>${first}</h2> 
<h2>${second}</h2> 
<h2>${third}</h2>`;
